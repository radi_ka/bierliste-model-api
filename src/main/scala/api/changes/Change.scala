package api.changes

/**
  * Used for storing incremental data within the observables
  * Sealed and final to enable compiler optimization and pattern matching
  *
  * Created by nephtys on 10/22/16.
  */
sealed trait Change[T]

final case class Creation[T](value : T) extends Change[T]

final case class Update[T](oldValue : T, newValue : T) extends Change[T]

final case class Deletion[T](value : T) extends Change[T]