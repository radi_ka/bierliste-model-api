package api

import api.changes.Change
import api.modelclasses.{FiscalBill, Item, User}
import io.reactivex.Observable

/**
  * Created by nephtys on 10/22/16.
  */
trait ObservablesAPI {


  def users : Observable[Change[User]]
  def items : Observable[Change[Item]]
  def bills : Observable[Change[FiscalBill]]

  //optional: purchases : Observable[Purchase], but that is probably better mapped inside the vm from users
}
