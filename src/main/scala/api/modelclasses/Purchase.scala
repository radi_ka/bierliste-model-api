package api.modelclasses

/**
  * Created by nephtys on 10/22/16.
  */
case class Purchase(
                     dateEpoch : Milliseconds, //moment of purchase (determined by viewmodel)
                    consumedItemsAndAmount : Map[Item, Int], //map with actual Items, as they may change over time
                    specialNotes : IndexedSeq[String], //users may remark something relevant
                     tags : Array[String] // 0..* tags given by the viewmodel. Perf. dep. on JVM string deduplication
                   )
{

}
