package api.modelclasses

import java.text.Normalizer
; /**
  * Created by nephtys on 10/22/16.
  */
case class User(
                 uuid : UUID[User],
                 name : String,
                 simplifiedName : Option[String],
                 isSpecialUser : Boolean,
                 purchasesSinceLastBill : IndexedSeq[Purchase], //TODO: make this upper bounded
                 openTags : Set[String],
                 unusedFreeItems : IndexedSeq[IssuedPersonalGift], //this should go til 0
                 ffaFreeItemsStillOpenForConsumption : IndexedSeq[IssuedFFA] //fast to 0
               ) {



  //sanity checks that need to be implemened in Command-Verifications
  assert(openTags.size <= 20) //more is bad for UI reasons and is dangerous
  assert(openTags.forall(_.length <= 140))
  assert(name.length <= 300)




  //translate ä to a and so on
  lazy val asciiName : String = simplifiedName.getOrElse({
    val subjectString = Normalizer.normalize(name, Normalizer.Form.NFD)
    subjectString.replaceAll("[^\\x00-\\x7F]", "")

  })
}
