package api.modelclasses

/**
  * Can be used as Actor Ref Hashvalues, as they are unique
  * Created by nephtys on 10/22/16.
  */
case class UUID[T](underlying : String) extends AnyVal {

}
